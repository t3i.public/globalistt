<h1>Hello Globalis !</h1>
<div>
    <a target="_blank" href="https://github.com/armel/GlobTest">Problem</a>
</div>
<div>
    <a target="_blank" href="https://gitlab.com/t3i.public/globalistt">Solution</a>
</div>

<h2>
    demo :
</h2>
<?php
$default = $_POST['input'] ?? '[[0, 3], [6, 10]]';
?>
<div>
    <form action="index.php" method="post">
        <label for="input">
            <textarea name="input" required><?php echo $default; ?></textarea>
        </label>
        <div>
            <button type="submit" name="run">Merge Intervals</button>
        </div>
    </form>
</div>

<pre>
<?php
include 'foo.php';

if (isset($_POST['input'])) {
	$intervals =  json_decode($_POST['input']);
	if (!$intervals) {
		echo 'invalid json !';
	}
	echo json_encode(foo($intervals));
}