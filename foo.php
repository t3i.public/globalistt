<?php
function foo($intervals = []) {

	usort($intervals, function ($a, $b) {
		return $a[0] - $b[0];
	});

	$response = [];
	$lastEnd = -1; // in 24H 0 is min

	foreach ($intervals as $interval) {
		$start = $interval[0];
		$end = $interval[1];

		if ($lastEnd < $start) {
			$response[] = [$start, $end];
		} else {
			$lastResponseIndex = array_key_last($response);
			$response[$lastResponseIndex][1] = max($response[$lastResponseIndex][1], $end);
		}

		$lastEnd = $end;
	}

	return $response;
}