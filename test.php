<?php

include 'foo.php';
var_dump([
	json_encode(foo([[0, 3], [6, 10]])) == json_encode([[0, 3], [6, 10]]),
	json_encode(foo([[0, 5], [3, 10]])) == json_encode([[0, 10]]),
	json_encode(foo([[0, 5], [2, 4]])) == json_encode([[0, 5]]),
	json_encode(foo([[7, 8], [3, 6], [2, 4]])) == json_encode(	[[2, 6], [7, 8]]),
	json_encode(foo([[3, 6], [3, 4], [15, 20], [16, 17], [1, 4], [6, 10], [3, 6]])) == json_encode(	[[1, 10], [15, 20]]),
]);
